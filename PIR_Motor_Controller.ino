//untuk mendeteksi gerekan agar robot tidak berkontak dengan robot lain
const int pirPin=9; //deklarasi var pin pir pada pin 9
const int motorPin=2; //deklarasi var pin motor pada pin 2
int switchstate = 0;
  
void setup()
{
  pinMode(pirPin, INPUT); //sensor pir sebagai input
  pinMode(motorPin, OUTPUT); //motor sebagai car output
}

void loop()
{
  switchstate = digitalRead (pirPin); //pir pin value di simpan dalam switchstate
    if (switchstate == HIGH)
    {
      //high means pir detect a motion
      digitalWrite (motorPin, LOW);
    }
    else
    {
      //low means there's no motion detected
      digitalWrite (motorPin, HIGH);
    }
}
